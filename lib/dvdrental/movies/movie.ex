defmodule Dvdrental.Movies.Movie do
  use Ecto.Schema
  import Ecto.Changeset

  schema "movies" do
    field :rental_rate, :integer
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(movie, attrs) do
    movie
    |> cast(attrs, [:title, :rental_rate])
    |> validate_required([:title, :rental_rate])
  end
end
