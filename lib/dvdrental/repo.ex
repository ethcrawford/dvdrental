defmodule Dvdrental.Repo do
  use Ecto.Repo,
    otp_app: :dvdrental,
    adapter: Ecto.Adapters.Postgres
end
