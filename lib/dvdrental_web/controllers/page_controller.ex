defmodule DvdrentalWeb.PageController do
  use DvdrentalWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
