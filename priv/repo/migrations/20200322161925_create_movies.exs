defmodule Dvdrental.Repo.Migrations.CreateMovies do
  use Ecto.Migration

  def change do
    create table(:movies) do
      add :title, :string
      add :rental_rate, :integer

      timestamps()
    end

  end
end
