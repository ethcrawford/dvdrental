# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :dvdrental,
  ecto_repos: [Dvdrental.Repo]

# Configures the endpoint
config :dvdrental, DvdrentalWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "IDWUg7Rb4dg2DKI5owWa6sKs/dpA+eKuLnD9qHd4ny8tp+J3C8JLM66GCCaWxeCB",
  render_errors: [view: DvdrentalWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Dvdrental.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "I3monpWp"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
